<?php

namespace bdb\menu;


use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;


/**
 * Menu renders a menu jQuery UI widget.
 *
 * @see https://github.com/bandodebits/yii2-bdb-menu
 * @author Bando de Bits <developer@bandodebits.com>
 * @since 1.0
 */
class Menu extends \yii\widgets\Menu
{
    /**
     * @var array the options for the underlying jQuery UI widget.
     * Please refer to the corresponding jQuery UI widget Web page for possible options.
     * For example, [this page](http://api.jqueryui.com/accordion/) shows
     * how to use the "Accordion" widget and the supported options (e.g. "header").
     */
    //public $clientOptions = [];
    /**
     * @var array the event handlers for the underlying jQuery UI widget.
     * Please refer to the corresponding jQuery UI widget Web page for possible events.
     * For example, [this page](http://api.jqueryui.com/accordion/) shows
     * how to use the "Accordion" widget and the supported events (e.g. "create").
     */
    //public $clientEvents = [];


    /*
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
    
    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    } */

    /*
     * Renders the widget.
     
    public function run()
    {
        parent::run();

        $view = $this->getView();

        $id = $this->options['id'];
        if ($this->clientOptions !== false) {
            $options = empty($this->clientOptions) ? '' : Json::encode($this->clientOptions);
            $js = "jQuery('#$id').menu($options);";
            $view->registerJs($js);
        }

        if (!empty($this->clientEvents)) {
            $js = [];
            foreach ($this->clientEvents as $event => $handler) {
                $js[] = "jQuery('#$id').on('menu$event', $handler);";
            }
            $view->registerJs(implode("\n", $js));
        }
    }*/

    public $linkTemplate = '<a {onclick} href="{url}" {dataAttributes}>{label}</a>';

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item)
    {
        //if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            return strtr($template, [
                '{onclick}' => isset($item['onclick']) ?  "onclick='javascript: ".$item['onclick']['function']."(".(isset($item['onclick']['options']) ? json_encode($item['onclick']['options']) : "{}")."); return false;'" : "",
                '{url}' => isset($item['url']) ? Html::encode(Url::to($item['url'])) : "",
                '{dataAttributes}' => isset($item['dataAttributes']) ? $item['dataAttributes'] : "",
                '{label}' => $item['label'],
            ]);
       /* } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
            ]);
        }*/


    }
}
